from sqlalchemy import Column, Integer, String
from sqlalchemy.ext.declarative import declarative_base


Base = declarative_base()


class LessonsIds(Base):
    __tablename__ = 'Lessons ids'
    id = Column(Integer, primary_key=True)
    lesson_id = Column(String(255))
    lesson_name = Column(String(255))