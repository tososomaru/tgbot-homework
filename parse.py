from selenium import webdriver 
from bs4 import BeautifulSoup
from selenium.webdriver.common.keys import Keys
import time
import re


def get_text(element, tag, class_):

    for element in element.find_all(tag, class_=class_):
        text = ''.join(element.findAll(text=True))
        return text


options = webdriver.FirefoxOptions()
options.headless = True
driver = "/home/tososomaru/homework/firefoxdriver/geckodriver"
browser = webdriver.Firefox(executable_path=driver,options=options)

browser.get('https://rasp.omgtu.ru/ruz/main')
browser.implicitly_wait(5)
try:
    group = browser.find_element_by_id("autocomplete-group")
    group.send_keys("АТП-171\n")
    time.sleep(1)
    group.send_keys(Keys.ENTER)
    time.sleep(1)
    generated_html = browser.page_source
    print('OK')
except Exception as ex:
    print(ex)

browser.quit()

if generated_html:
    soup = BeautifulSoup(generated_html, 'html.parser')
    for paare in soup.find_all('div', class_="list ng-star-inserted"):
        for elem in soup.find_all('div', class_="media day ng-star-inserted"):

            week = get_text(elem, tag='div', class_='week')
            day = get_text(elem, tag='div', class_='day')
            month = get_text(elem, tag='div', class_='month')
            time = get_text(elem, tag='div', class_='time')
            paare_count = get_text(elem, tag='small', class_='')
            for title in elem.find_all('div', class_="title"):
                paare_name = get_text(title, tag='span', class_='')
                paare_type = get_text(title, tag='div', class_='text-muted kind ng-star-inserted')
            auditorium = get_text(elem, tag='span', class_='auditorium')
            location = get_text(elem, tag='span', class_='mr-2 text-muted ng-star-inserted')
            subgroup = get_text(elem, tag='div', class_='subGroup ng-star-inserted')
            lecturer = get_text(elem, tag='div', class_='lecturer')
            
            print(
                f"Неделя {week} День {day} Месяц {month} Время {time} Номер пары {paare_count}"
                f"Пара {paare_name} Тип {paare_type} Аудитория {auditorium} Расположение {location}"
                f"Подгруппа {subgroup} Преподаватель {lecturer}"
                )
    