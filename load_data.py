import os
import asyncio
import logging

from aiogram import Bot
from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
from dotenv import load_dotenv

from models import Base


load_dotenv()

DB_FILENAME = os.getenv("DB_FILENAME")
TOKEN = os.getenv("TELEGRAM_API_TOKEN")

engine = create_engine(f'sqlite:///{DB_FILENAME}')

if not os.path.isfile(f'./{DB_FILENAME}'):
    Base.metadata.create_all(engine)


session_factory = sessionmaker(bind=engine)
Session = scoped_session(session_factory)

bot = Bot(token=TOKEN)




# async def uploadMediaFiles(folder, method, file_attr):
#     folder_path = os.path.join(BASE_MEDIA_PATH, folder)
#     for filename in os.listdir(folder_path):
#         if filename.startswith('.'):
#             continue

#         logging.info(f'Started processing {filename}')
#         with open(os.path.join(folder_path, filename), 'rb') as file:
#             msg = await method(MY_ID, file, disable_notification=True)
#             if file_attr == 'photo':
#                 file_id = msg.photo[-1].file_id
#             else:
#                 file_id = getattr(msg, file_attr).file_id
#             session = Session()
#             newItem = MediaIds(file_id=file_id, filename=filename)
#             try:
#                 session.add(newItem)
#                 session.commit()
#             except Exception as e:
#                 logging.error(
#                     'Couldn\'t upload {}. Error is {}'.format(filename, e))
#             else:
#                 logging.info(
#                     f'Successfully uploaded and saved to DB file {filename} with id {file_id}')
#             finally:
#                 session.close()